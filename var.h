#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

enum {
    var_type_string,
    var_type_node
};

typedef struct var_tag var_t;

typedef struct {
    int len;
    const char *text;
} string_t;

typedef struct {
    var_t *data;
    var_t *next;
} node_t;

struct var_tag {
    int type;
    union {
        string_t string;
        node_t   node;
    };
};
var_t * var_string(int len, const char *text)
{
    var_t *v = (var_t*)malloc(sizeof(var_t));

    if(v) {
        char *t = (char*)malloc(len + 1);
        v->type = var_type_string;
        if(t) {
            v->string.len = len;
            memcpy(t, text, len);
            t[len] = 0;
            v->string.text = t;
        } else {
            free(v);
            v = NULL;
        }
    }
    return v;
}

var_t * var_node(void *data)
{
    var_t *v = (var_t*)malloc(sizeof(var_t));
    if(v) {
        v->type = var_type_node;
        v->node.data = (var_t*)data;
        v->node.next = NULL;
    }
    return v;
}

void spaces(int n)
{
    int i;
    for(i = 0;i < n;i++) {
        printf(" ");
    }
}

void var_dump_(var_t *v, int depth)
{
    if(v == NULL) {
        printf("null\n");
        return;
    }
    switch(v->type) {
        case var_type_string:
            spaces(depth*2);
            printf("string %3d '%s'\n",v->string.len, v->string.text);
            break;
        case var_type_node:
            spaces(depth*2);
            printf("list\n");
            ++depth;
            do {
                var_t *d = v->node.data;
                if(d) var_dump_(d, depth);
                else {
                    spaces(depth*2);
                    printf("nothing\n");
                }
                v = v->node.next;
            } while(v);
            break;
        default:
            spaces(depth*2);
            printf("???\n");
            break;
    } 
}

void var_dump(var_t *v)
{
    var_dump_(v, 0);
}

void var_debug(var_t *v)
{
    printf("%p ",v);
    if(v) {
        printf("type=%d ",v->type);
    }
    printf("\n");
}

void var_free(var_t *v)
{
    if(v) {
        switch(v->type) {
//            case var_type_string:
//                free(v->string.text);
            // TODO
            
        }
        free(v);
    }
}

