#include <iostream>
#include <vector>


class Elem {
public:
    virtual ~Elem() = default;
    virtual void show() = 0;
    virtual bool iterable() = 0;
};


class Word : public Elem {
public:
    std::string str;

    Word(int len, const char *s) {
        char *tmp = (char*)malloc(len + 1);
        memcpy(tmp, s, len);
        tmp[len] = 0;
        str = tmp;
        free(tmp);
    }

    ~Word() {
        std::cout << "destroying Word " << str << std::endl;
    }

    void show() {
        std::cout << str << " ";
    }

    bool iterable() {
        return false;
    }
};


class List : public Elem {
public:
    std::vector<std::shared_ptr<Elem>> elems;

    ~List() {
        std::cout << "Destroying List ";
        show();
        std::cout << std::endl;
        //for(auto e : elems) {
        //    delete e;
        //}
    }

    void add(std::shared_ptr<Elem> e) {
        elems.push_back(e);
    }

    void show() {
        std::cout << "[ ";
        for(auto e : elems) {
            e->show();
        }
        std::cout << "] ";
    }

    bool iterable() {
        return true;
    }
};

#if 0
int main(void)
{
    auto w = Word(5, "Hello world");
    auto l = List();
    auto k = List();
    l.add(&w);
    l.add(&w);
    l.add(&k);
    std::cout << sizeof(w) << std::endl;
    std::cout << sizeof(l) << std::endl;
    std::cout << l.elems.size() << std::endl;
    w.show();
    std::cout << std::endl;
    l.show();
    std::cout << std::endl;


    Elem& e = w;
    Elem& f = l;
    e.show();
    std::cout << std::endl;
    f.show();
    std::cout << std::endl;

    return 0;
}
#endif

