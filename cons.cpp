// functional cons, car, cdr
//
// g++ -std=c++17 cons.cpp


#include <iostream>


typedef int var;

auto cons(var a, var b)
{
    return [=](int x) { return x ? b : a; };
}

typedef decltype(cons(0, 0)) cons_type;

auto car(cons_type c)
{
    return c(0);
}

auto cdr(cons_type c)
{
    return c(1);
}

int main(void)
{
    auto c = cons(123, 999);
    auto d = cons(12, 99);
    std::cout << car(c) << std::endl;
    std::cout << cdr(c) << std::endl;
    std::cout << car(d) << std::endl;
    std::cout << cdr(d) << std::endl;
    return 0;
}
