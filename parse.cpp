// parse lisp
//
// g++ -std=c++17 parse.cpp

#include <iostream>
#include <memory>

#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#include "elem.h"

#define WSPACE " \t\n\v\f\r"

// skip whitespace and return the first non-ws char
static char ws(const char **src)
{
    char c;
    *src += strspn(*src, WSPACE);
    c = *(*src)++;
    if(c != '(' && c != ')') --(*src);
    return c;
}

std::shared_ptr<Elem> parse_word(const char **src)
{
    const char *start = *src;
    *src += strcspn(*src, WSPACE ")");
    return std::make_shared<Word>(*src - start, start);
}

std::shared_ptr<Elem> parse_item(const char **src, int *depth);

std::shared_ptr<List> parse_list(const char **src, int *depth)
{
    std::shared_ptr<List> list = std::make_shared<List>();
    ++(*depth);
    for(;;) {
        auto e = parse_item(src, depth);
        if(!e) break;
        list->add(e);
    }
    return list;
}

std::shared_ptr<Elem> parse_item(const char **src, int *depth)
{
    char c = ws(src);
    if(c == 0)        {return NULL;}        // error?
    else if(c == ')') {--(*depth); return NULL;}
    else if(c == '(') return parse_list(src, depth);
    else              return parse_word(src);
}

void go(const char *txt)
{
    int error = 0;
    std::cout << "source: '" << txt << "'" << std::endl;
    std::shared_ptr<Elem> t = parse_item(&txt, &error);
    if(t)
        t->show();
    else
        std::cout << "NULL" << std::endl;
    std::cout << std::endl << "error: " << error;
    std::cout << " unparsed: '" << txt << "'" << std::endl << std::endl;
}

int main(void)
{
    go("((lambda  (arg) (+ arg  1)) 5)");
    go("dog cat");
    go("symbol ");
    go("((λ (x) (+ x 1)) 5)");
    go("(dog (cat))");
    go("(dog (cat");
    go("");
    return 0;
}
